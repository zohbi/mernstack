const express       = require('express')
const mongoose      = require('mongoose')
const bodyParser    = require('body-parser')

// API Variables
const items = require('./routes/api/items')

const app = express()
// bodyParser middleware
app.use(bodyParser.json())

// Db Config
const db = require('./config/keys').mongoURI

mongoose.connect(db)
    .then(()=> console.log('Db connected'))
    .catch(err=> console.log(err))

const port = process.env.PORT || 5000

app.listen(port, ()=>console.log(`Server started on port ${port}`))

app.use('/api/items', items)
    
